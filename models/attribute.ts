import { Model } from './model.ts'

export class Attribute extends Model {
    id = 0
    name = ''

    static getTableName() {
        return 'attribute'
    }
}
