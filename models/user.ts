import { Model } from './model.ts'

export class User extends Model {
    id = 0
    firstName = ''
    lastName = ''
    birthday = 0
    email = ''

    static getTableName() {
        return 'user'
    }
}
