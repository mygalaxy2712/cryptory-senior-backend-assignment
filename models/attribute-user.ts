import { Model } from './model.ts'

export class AttributeUser extends Model {
    attributeId = 0
    userId = 0

    static getTableName() {
        return 'attribute_user'
    }
}
