import { QueryBuilder } from '../app/query-builder.ts'

export class Model {
    constructor() {}

    static buildQuery() {
        return QueryBuilder.build()
            .select(this.getKeys())
            .from(this.getTableName())
    }

    static fromMap(_map: Map<string, unknown>) {
        const instance = new this()
        for (const k of Object.keys(instance)) {
            const f = k as keyof Model
            const km = this.toCamelCase(k)
            instance[f] = _map.get(km) as never
        }

        return instance
    }

    static toCamelCase(s: string) {
        return s.replace(/[A-Z]/, (v) => `_${v.toLowerCase()}`)
    }

    static getKeys() {
        const instance = new this()

        return Object.keys(instance).map((k) => this.toCamelCase(k))
    }

    static getTableName() {
        return ''
    }

    static total() {
        return (
            (QueryBuilder.build()
                .select(['count(*)'])
                .from(this.getTableName())
                .query()[0]
                .get('count(*)') as number) ?? 0
        )
    }
}
