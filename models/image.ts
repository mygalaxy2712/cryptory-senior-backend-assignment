import { Model } from './model.ts'

export class Image extends Model {
    id = ''
    name = ''
    created = 0
    height = 0
    width = 0
    userId = 0

    static getTableName() {
        return 'image'
    }
}
