import { log } from '../../app/middleware.ts'
import { RouterContext } from '../../deps.ts'
import * as service from '../../services/index.ts'

export const list = ({ request, response }: RouterContext<string>) => {
    const offset = Number(request.url.searchParams.get('offset') ?? 0)
    const limit = Number(request.url.searchParams.get('limit') ?? 10)
    response.body = JSON.stringify(service.user.getUsers({ offset, limit }))
}
