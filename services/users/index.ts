import { Attribute, AttributeUser, Image, User } from '../../models/index.ts'
import { PaginationInfo } from '../../utils/index.ts'

const queryUsers = (paginate: PaginationInfo) => {
    return User.buildQuery()
        .limit(paginate.limit)
        .offset(paginate.offset)
        .query()
        .map((r) => User.fromMap(r) as User)
}

const getImagesToUserMap = (userIds: number[]) => {
    const images = Image.buildQuery()
        .where(`user_id IN (${userIds.join(', ')})`)
        .orderBy('created')
        .query()
        .map((r) => Image.fromMap(r) as Image)

    const imagesToUserMap = new Map<number, Image[]>()

    for (const i of images) {
        const map: Image[] = imagesToUserMap.get(i.userId) ?? []
        map.push(i)
        imagesToUserMap.set(i.userId, map)
    }

    return imagesToUserMap
}

const getAusToUserMapAndAttributesMap = (userIds: number[]) => {
    const aus = AttributeUser.buildQuery()
        .where(`user_id IN (${userIds.join(', ')})`)
        .query()
        .map((r) => AttributeUser.fromMap(r) as AttributeUser)

    const ausToUserMap = new Map<number, number[]>()
    const attributeIds = new Set<number>()

    for (const au of aus) {
        const map: number[] = ausToUserMap.get(au.userId) ?? []
        map.push(au.attributeId)
        ausToUserMap.set(au.userId, map)
        attributeIds.add(au.attributeId)
    }

    const attributes = Attribute.buildQuery()
        .where(`id IN (${Array.from(attributeIds).join(', ')})`)
        .query()
        .map((r) => Attribute.fromMap(r) as Attribute)

    const attributesById = new Map<number, Attribute>()

    for (const a of attributes) {
        attributesById.set(a.id, a)
    }

    return { attributesById, ausToUserMap }
}

export const getUsers = (paginate: PaginationInfo) => {
    const totalUser = User.total()

    const users = queryUsers(paginate)

    const userIds = users.map((u) => u.id)

    const imagesToUserMap = getImagesToUserMap(userIds)

    const { attributesById, ausToUserMap } =
        getAusToUserMapAndAttributesMap(userIds)

    const data = users.map((u) => {
        const um = new Map(Object.entries(u))

        um.set('media', imagesToUserMap.get(u.id))

        um.set(
            'attributes',
            ausToUserMap.get(u.id)?.map((ai) => attributesById.get(ai))
        )

        return Object.fromEntries(um)
    })

    return {
        data,
        total_user: totalUser,
        total_page: Math.ceil(totalUser / paginate.limit),
        offset: paginate.offset,
        limit: paginate.limit,
    }
}
